'use client';

import { useRouter } from 'next/navigation';
import { useAppDispatch } from '@/shared/setup/hooks';
import { gameBasicActions } from '@/shared/modules/game/game.slice';

import styles from './page.module.css';
import Image from 'next/image';

import { Button } from '@/shared/UI/button';

export default function Home() {
  const router = useRouter();
  const dispatch = useAppDispatch();

  const onClickBtn = () => {
    dispatch(gameBasicActions.startGameInProgress());
    router.replace('/game');
  };

  return (
    <main className={styles.main}>
      <Image src="/hand.svg" alt="hand" width={1} height={1} className={styles.image} />
      <div className={styles.wrapper}>
        <p className={styles.title}>Who wants to be a millionaire?</p>
        <Button text="start" onClick={onClickBtn} />
      </div>
    </main>
  );
}
