'use client';
import { useRouter } from 'next/navigation';

import styles from './page.module.css';
import Image from 'next/image';

import { useAppSelector } from '@/shared/setup/hooks';
import { useAppDispatch } from '@/shared/setup/hooks';

import { Button } from '@/shared/UI/button';
import { gameBasicActions } from '@/shared/modules/game/game.slice';
import { gameSelectors } from '@/shared/modules/game/game.selector';

export default function Finish() {
  const router = useRouter();
  const dispatch = useAppDispatch();

  const previousQuestionData = useAppSelector(gameSelectors.previousQuestionData);

  const onClickBtn = () => {
    dispatch(gameBasicActions.startGameInProgress());
    router.replace('/game');
  };

  return (
    <main className={styles.main}>
      <Image src="/hand.svg" alt="hand" width={1} height={1} className={styles.image} />
      <div className={styles.wrapper}>
        <div>
          <p className={styles.title}>Total score:</p>
          <p className={styles.result}>{previousQuestionData?.bid || '0$'} earned</p>
        </div>
        <Button text="try again" onClick={onClickBtn} />
      </div>
    </main>
  );
}
