'use client';

import React, { useEffect, useState } from 'react';
import { useAppDispatch } from '@/shared/setup/hooks';
import { useAppSelector } from '@/shared/setup/hooks';
import { useMediaQuery } from '@/shared/setup/hooks';

import { QuestionDetails } from '@/shared/components/question-details';
import { QuestionsList } from '@/shared/components/questions-list';
import { Loader } from '@/shared/UI/loader';
import { BurgerMenu } from '@/shared/UI/burger-menu';

import styles from './page.module.css';
import { gameBasicActions } from '@/shared/modules/game/game.slice';
import { useRouter } from 'next/navigation';

export default function Game() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const gameStatus = useAppSelector((state) => state.gameReducer.status);

  const isTablet = useMediaQuery('(max-width: 520px)');

  const { loading } = useAppSelector((state) => state.questionReducer);

  const [toggleScreen, setToggleScreen] = useState<boolean>(false);

  useEffect(() => {
    if (gameStatus === 'finished') {
      router.replace('/finish');
    }
  }, [gameStatus]);

  useEffect(() => {
    dispatch(gameBasicActions.startGame());
  }, []);

  const handleToggleScreen = () => {
    setToggleScreen((state) => !state);
  };

  const mobileView = () => (
    <div className={styles.contentWrapper}>
      <div className={styles.menuWrapper}>
        <BurgerMenu checked={toggleScreen} onChange={handleToggleScreen} />
      </div>
      {toggleScreen ? <QuestionsList /> : <QuestionDetails />}
    </div>
  );

  const desktopView = () => (
    <>
      <QuestionDetails />
      <QuestionsList />
    </>
  );

  return <main className={styles.main}>{loading ? <Loader /> : <>{isTablet ? mobileView() : desktopView()}</>}</main>;
}
