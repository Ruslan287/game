import styles from './burger-menu.module.css';

interface IBurgerMenuProps {
  checked: boolean;
  onChange: () => void;
}

export const BurgerMenu: React.FC<IBurgerMenuProps> = ({ checked, onChange }) => {
  return (
    <div className={styles.root}>
      <input type="checkbox" checked={checked} onChange={onChange} />

      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};
