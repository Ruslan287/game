import styles from './button.module.css';

interface IButtonProps {
  text: string;
  onClick: () => void;
}

export const Button: React.FC<IButtonProps> = ({ text, onClick }) => {
  return (
    <button type="button" className={styles.root} onClick={onClick}>
      {text}
    </button>
  );
};
