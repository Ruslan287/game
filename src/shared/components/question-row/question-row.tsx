'use client';

import { EntityId } from '@reduxjs/toolkit';
import styles from './question-row.module.css';

import { useAppSelector } from '@/shared/setup/hooks';
import { questionsBasicSelectors } from '@/shared/modules/question/question.slice';
import { gameSelectors } from '@/shared/modules/game/game.selector';

interface IQuestionRowProps {
  id: EntityId;
}

export const QuestionRow: React.FC<IQuestionRowProps> = ({ id }) => {
  const currentStep = useAppSelector(gameSelectors.currentStep) + 1;
  const questionData = useAppSelector((store) => questionsBasicSelectors.selectById(store, id));

  const getStyles = () => {
    switch (true) {
      case currentStep === Number(questionData?.step):
        return styles.currentQuestion;
      case currentStep > Number(questionData?.step):
        return styles.previousQuestion;
      case currentStep < Number(questionData?.step):
        return styles.nextQuestion;
    }
  };

  return (
    <div className={styles.root}>
      <div className={`${styles.box} ${getStyles()}`}>{questionData?.bid}</div>
    </div>
  );
};
