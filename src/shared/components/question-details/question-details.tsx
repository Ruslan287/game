'use client';

import styles from './question-details.module.css';

import { useAppSelector } from '@/shared/setup/hooks';

import { AnswerRow } from '@/shared/components/answer-row';
import { gameSelectors } from '@/shared/modules/game/game.selector';

export const QuestionDetails: React.FC = () => {
  const questionData = useAppSelector(gameSelectors.currentQuestionData);

  return (
    <div className={styles.root}>
      <div className={styles.question}>{questionData?.text}</div>
      <div>
        {questionData?.answers.map((id) => (
          <AnswerRow id={id} key={id} />
        ))}
      </div>
    </div>
  );
};
