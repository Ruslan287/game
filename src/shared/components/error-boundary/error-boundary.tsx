'use client';

import React from 'react';

import styles from './error-boundary.module.css';

import { Button } from '@/shared/UI/button';

interface Props {
  children?: React.ReactNode;
}

interface State {
  hasError: boolean;
}

export class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    console.log({ error, errorInfo });
    this.setState((state) => ({ ...state, hasError: true }));
  }

  onClickBtn = () => {
    window.location.reload();
  };

  render() {
    if (this.state.hasError) {
      return (
        <main className={styles.root}>
          <div>
            <h2 className={styles.title}>Oops, there is an error!</h2>

            <Button text="reload" onClick={this.onClickBtn} />
          </div>
        </main>
      );
    }

    return this.props.children;
  }
}
