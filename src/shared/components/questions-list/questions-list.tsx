'use client';

import styles from './questions-list.module.css';

import { useAppSelector } from '@/shared/setup/hooks';

import { QuestionRow } from '../question-row';
import { gameSelectors } from '@/shared/modules/game/game.selector';

export const QuestionsList: React.FC = () => {
  const questionsIds = useAppSelector(gameSelectors.questionsIdsReversed);

  return (
    <div className={styles.root}>
      {questionsIds.map((questionId) => (
        <QuestionRow id={questionId} key={questionId} />
      ))}
    </div>
  );
};
