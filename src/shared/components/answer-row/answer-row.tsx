'use client';

import { EntityId } from '@reduxjs/toolkit';
import styles from './answer-row.module.css';

import { useAppSelector } from '@/shared/setup/hooks';
import { useAppDispatch } from '@/shared/setup/hooks';
import { answersBasicSelectors } from '@/shared/modules/answer/answer.slice';
import { gameBasicActions } from '@/shared/modules/game/game.slice';

interface IAnswerRowProps {
  id: EntityId;
}

export const AnswerRow: React.FC<IAnswerRowProps> = ({ id }) => {
  const dispatch = useAppDispatch();

  const answerData = useAppSelector((store) => answersBasicSelectors.selectById(store, id));
  const showResult = useAppSelector((state) => state.gameReducer.showResult);

  const handleAnswerChoice = () => {
    if (answerData) {
      dispatch(gameBasicActions.answerChoice(answerData));
    }
  };

  const getStyle = () => {
    return answerData?.correct ? showResult && styles.successful : showResult && styles.failed;
  };

  return (
    <button className={styles.wrapper} onClick={handleAnswerChoice} disabled={showResult}>
      <div className={`${styles.box} ${getStyle()}`}>
        <div className={styles.option}>{answerData?.option}</div>
        <div className={styles.value}>{answerData?.value}</div>
      </div>
    </button>
  );
};
