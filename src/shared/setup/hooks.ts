import { useEffect, useState, useMemo } from 'react';

import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { RootState, AppDispatch } from '@/shared/setup/store';

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export const useMediaQuery = (query: string) => {
  const mediaQuery = useMemo(() => {
    if (typeof window !== 'undefined') {
      return window?.matchMedia(query);
    }
  }, [query]);
  const [match, setMatch] = useState(mediaQuery?.matches);

  useEffect(() => {
    const onChange = () => setMatch(mediaQuery?.matches);
    mediaQuery?.addEventListener('change', onChange);

    return () => mediaQuery?.removeEventListener('change', onChange);
  }, [mediaQuery]);

  return match;
};
