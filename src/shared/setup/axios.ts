import axios from 'axios';

const baseURL = `${process.env.API_HOST}`;

const axiosInstance = axios.create({ baseURL });

export default axiosInstance;
