import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { all, call } from 'redux-saga/effects';

import questionSaga from '@/shared/modules/question/question.saga';
import questionReducer from '@/shared/modules/question/question.slice';
import answerReducer from '@/shared/modules/answer/answer.slice';
import gameReducer from '@/shared/modules/game/game.slice';
import gameSaga from '@/shared/modules/game/game.saga';

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: { questionReducer, answerReducer, gameReducer },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
});

function* rootSaga() {
  yield all([call(questionSaga), call(gameSaga)]);
}

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
