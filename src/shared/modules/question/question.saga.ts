import { put, takeLatest } from 'redux-saga/effects';
import { questionsBasicActions } from '@/shared/modules/question/question.slice';
import { answersBasicActions } from '@/shared/modules/answer/answer.slice';
import axiosInstance from '@/shared/setup/axios';

import { IAnswer } from '@/shared/modules/answer/answer.types';
import { IApiQuestion, IQuestion } from '@/shared/modules/question/question.types';

interface INormalizedQuestionsList {
  answers: IAnswer[];
  questions: IQuestion[];
}

interface INormalizedQuestionsData {
  question: IQuestion;
  answers: IAnswer[];
}

export const normalizeQuestionData = (data: IApiQuestion): INormalizedQuestionsData => {
  const { answers } = data;
  const question = { ...data, answers: answers.map(({ id }) => id) };
  return { question, answers };
};

export const normalizeQuestionsList = (data: IApiQuestion[]): INormalizedQuestionsList => {
  return data.reduce<INormalizedQuestionsList>(
    (acc, item) => {
      const normalizedItem = normalizeQuestionData(item);
      return {
        ...acc,
        answers: [...acc.answers, ...normalizedItem.answers],
        questions: [...acc.questions, { ...item, answers: normalizedItem.question.answers }],
      };
    },
    { answers: [], questions: [] }
  );
};

export function* getQuestionsSaga() {
  yield put(questionsBasicActions.startLoading());

  const { data }: { data: { questions: Array<IApiQuestion> } } = yield axiosInstance.get('/api/questions');

  const { questions, answers } = normalizeQuestionsList(data.questions);

  yield put(questionsBasicActions.getAllQuestionsSuccess(questions));
  yield put(answersBasicActions.getAllAnswersSuccess(answers));

  yield put(questionsBasicActions.completeLoading());
}

export default function* questionSaga() {
  yield takeLatest(questionsBasicActions.getAllQuestions, getQuestionsSaga);
}
