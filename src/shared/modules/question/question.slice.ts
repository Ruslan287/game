import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { RootState } from '@/shared/setup/store';
import { IQuestion } from './question.types';

export const questionsAdapter = createEntityAdapter<IQuestion>();

const questionSlice = createSlice({
  name: 'questionsSlice',
  initialState: questionsAdapter.getInitialState({ loading: false }),
  reducers: {
    getAllQuestions: () => {},
    getAllQuestionsSuccess: questionsAdapter.setMany,
    startLoading: (state) => {
      state.loading = true;
    },
    completeLoading: (state) => {
      state.loading = false;
    },
  },
});

export default questionSlice.reducer;
export const questionsBasicActions = questionSlice.actions;
export const questionsBasicSelectors = questionsAdapter.getSelectors<RootState>((state) => state.questionReducer);
