import { IQuestion } from '@/shared/modules/question/question.types';

type TGameStatus = 'idle' | 'in_progress' | 'finished';
export interface IGameSlice {
  status: TGameStatus;
  currentQuestionId: IQuestion['id'];
  lastAnsweredQuestionId: IQuestion['id'];
  questionsIds: Array<IQuestion['id']>;
  showResult: boolean;
}
