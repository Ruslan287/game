import { IAnswer, TAnswerId } from '@/shared/modules/answer/answer.types';
import { Modify } from '@/shared/tools/types';

export interface IApiQuestion extends Modify<IQuestion, { answers: Array<IAnswer> }> {}

export interface IQuestion {
  id: number;
  step: number;
  text: string;
  bid: string;
  answers: Array<TAnswerId>;
}
