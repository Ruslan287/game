import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { RootState } from '@/shared/setup/store';
import { IAnswer } from './answer.types';

const answerAdapter = createEntityAdapter<IAnswer>();

const answerSlice = createSlice({
  name: 'answerSlice',
  initialState: answerAdapter.getInitialState(),
  reducers: {
    getAllAnswersSuccess: answerAdapter.setMany,
  },
});

export default answerSlice.reducer;
export const answersBasicActions = answerSlice.actions;
export const answersBasicSelectors = answerAdapter.getSelectors<RootState>((state) => state.answerReducer);
