export interface IAnswer {
  id: number;
  correct: boolean;
  value: string;
  option: string;
}

export type TAnswerId = IAnswer['id'];
