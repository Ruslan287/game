import { put, select, takeLatest } from 'redux-saga/effects';
import { gameBasicActions } from '@/shared/modules/game/game.slice';
import { call, delay } from '@redux-saga/core/effects';
import { questionsBasicSelectors } from '@/shared/modules/question/question.slice';
import { getQuestionsSaga } from '@/shared/modules/question/question.saga';
import { gameSelectors } from '@/shared/modules/game/game.selector';

function* startGameSaga() {
  yield call(getQuestionsSaga);

  const questions: ReturnType<typeof questionsBasicSelectors.selectAll> = yield select(
    questionsBasicSelectors.selectAll
  );
  const questionsIds = questions
    .slice()
    .sort((a, b) => a.step - b.step)
    .map(({ id }) => id);
  const currentQuestionId = questionsIds[0];

  yield put(gameBasicActions.startGameComplete({ currentQuestionId, questionsIds }));
}

function* answerChoiceSaga({ payload }: ReturnType<typeof gameBasicActions.answerChoice>) {
  yield put(gameBasicActions.setShowResult(true));
  yield delay(1500);
  yield put(gameBasicActions.setShowResult(false));

  if (payload.correct) {
    const questionsIds: ReturnType<typeof gameSelectors.questionsIds> = yield select(gameSelectors.questionsIds);
    const currentStep: ReturnType<typeof gameSelectors.currentStep> = yield select(gameSelectors.currentStep);
    const nextQuestionId = questionsIds[currentStep + 1];

    if (nextQuestionId !== undefined) {
      yield put(gameBasicActions.answerChoiceComplete(nextQuestionId));
    } else {
      const currentQuestionId: ReturnType<typeof gameSelectors.currentQuestionId> = yield select(
        gameSelectors.currentQuestionId
      );
      yield put(gameBasicActions.endGame(currentQuestionId));
    }
  } else {
    yield put(gameBasicActions.endGame());
  }
}

export default function* gameSaga() {
  yield takeLatest(gameBasicActions.startGame, startGameSaga);
  yield takeLatest(gameBasicActions.answerChoice, answerChoiceSaga);
}
