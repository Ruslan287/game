import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IGameSlice } from '@/shared/modules/question/game.types';
import { IAnswer } from '@/shared/modules/answer/answer.types';

const getInitialState = (): IGameSlice => ({
  status: 'idle',
  currentQuestionId: 0,
  lastAnsweredQuestionId: 0,
  questionsIds: [],
  showResult: false,
});

const gameSlice = createSlice({
  name: 'gameSlice',
  initialState: getInitialState(),
  reducers: {
    startGame: (state) => {},
    endGame: (state, { payload }: PayloadAction<IGameSlice['lastAnsweredQuestionId'] | undefined>) => {
      state.status = 'finished';
      if (payload) {
        state.lastAnsweredQuestionId = payload;
      }
    },
    startGameComplete: (
      state,
      {
        payload: { currentQuestionId, questionsIds },
      }: PayloadAction<Pick<IGameSlice, 'currentQuestionId' | 'questionsIds'>>
    ) => {
      state.lastAnsweredQuestionId = 0;
      state.currentQuestionId = currentQuestionId;
      state.questionsIds = questionsIds;
    },
    answerChoice: (state, action: PayloadAction<IAnswer>) => {},
    answerChoiceComplete: (state, { payload }: PayloadAction<IAnswer['id']>) => {
      state.lastAnsweredQuestionId = state.currentQuestionId;
      state.currentQuestionId = payload;
    },
    startGameInProgress: (state) => {
      state.status = 'in_progress';
    },
    setShowResult: (state, action) => {
      state.showResult = action.payload;
    },
  },
});

export default gameSlice.reducer;
export const gameBasicActions = gameSlice.actions;
