import {createSelector} from "@reduxjs/toolkit"
import {questionsBasicSelectors} from "@/shared/modules/question/question.slice"
import {RootState} from "@/shared/setup/store"

const reducer = (state: RootState) => state.gameReducer
const currentQuestionId = createSelector(reducer, state => state.currentQuestionId);
const questionsIds = createSelector(reducer, state => state.questionsIds);
const questionsIdsReversed = createSelector(reducer, state => state.questionsIds.slice().reverse());

const currentStep = createSelector(questionsIds, currentQuestionId, (questionsIds, currentQuestionId) => {
  return questionsIds.findIndex(id => id === currentQuestionId)
})

const currentQuestionData = createSelector(
  reducer,
  questionsBasicSelectors.selectEntities,
  (state, questions) => questions[state.currentQuestionId]
)

const previousQuestionData = createSelector(
  reducer,
  questionsBasicSelectors.selectEntities,
  (state, questions) => questions[state.lastAnsweredQuestionId]
)

export const gameSelectors = {
  currentStep,
  currentQuestionId,
  currentQuestionData,
  questionsIds,
  questionsIdsReversed,
  previousQuestionData,
}
